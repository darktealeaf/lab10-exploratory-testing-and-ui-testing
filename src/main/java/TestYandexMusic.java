import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestYandexMusic {

    @Test
    public void testYandexMusic() {
        WebDriver driver = new FirefoxDriver();
        driver.get("https://music.yandex.ru/home");
        System.out.println("Page Title is " + driver.getTitle());
        Assert.assertEquals(driver.getTitle(), "Яндекс.Музыка — собираем музыку и подкасты для вас");

        //Close the pop-up message
        WebElement header = driver.findElement(By.className("payment-plus__header"));
        WebElement closeButton = header.findElement(By.xpath("div/span"));
        closeButton.click();

        WebElement findLine = driver.findElement(By.className("deco-input_suggest"));
        findLine.sendKeys("Noize");

        WebElement searchButton = driver.findElement(By.className("d-button_type_flat"));
        searchButton.click();
        Assert.assertEquals(findLine.getAttribute("value"), "Noize");

        driver.get("https://music.yandex.ru/search?text=Noize");
        Assert.assertEquals(driver.getTitle(), "Noize: песни, альбомы, плейлисты");

        driver.quit();
    }
}
